/* roll.js
 Roll the dice! 
 https://en.wikipedia.org/wiki/Bo_Bing_(game) */

function randomNumber (minimum, maximum, times = 1) {
	let resultNumber = 0;
	
	for (currentRandom = 0; currentRandom < times; currentRandom++) {
		resultNumber = Math.floor(Math.random() * (maximum - minimum) + minimum);
	};
	return resultNumber;
};

function rollDice(times = 1) {
	let value_dices = [];
	let value_current = 0;

	let dices_amount_current = value_dices.length;

	for (rolls = 0; rolls < times; rolls++) {
		for (dices_max = 6; dices_amount_current < dices_max; dices_amount_current = value_dices.length) {
			value_current = randomNumber(1, 6);
			
			if (randomNumber(1, 2) == 1) {
				value_dices.push(value_current);
			} else {
				value_dices.unshift(value_current);
			};
		};
	};

	return value_dices;
};

function rollDiceWithResults() {
	let value_dices = rollDice();
	let value_current = 0;
	let value_dices_numbers = {
		'one': 0,
		'two': 0,
		'three': 0,
		'four': 0,
		'five': 0,
		'six': 0
	};
	
	/* The first dice is always the zeroth dice! */
	for (diceNumber = 0; diceNumber < value_dices.length; diceNumber++) {
		value_current = value_dices[diceNumber];
		switch (value_current) {
			case 1: value_dices_numbers.one++; break;
			case 2: value_dices_numbers.two++; break;
			case 3: value_dices_numbers.three++; break;
			case 4: value_dices_numbers.four++; break;
			case 5: value_dices_numbers.five++; break;
			case 6: value_dices_numbers.six++; break;
		};
	};
	
	let result_number = 0;
	/* The highest of each place starts as an integer, the rest is an increment by .1. */
	if ((value_dices_numbers.four == value_dices.length) || (value_dices_numbers.one == value_dices.length)) {
		/* Six Fours */
		result_number = 1;
		if (value_dices_numbers.one > value_dices_numbers.four) {
			/* Six Ones */
			result_number = result_number + 0.1;
		};
	} else if ((value_dices_numbers.two == value_dices.length) || (value_dices_numbers.three == value_dices.length) || (value_dices_numbers.five == value_dices.length) || (value_dices_numbers.six == value_dices.length)) {
		/* Six of a Kind */
		result_number = 1.2;
	} else if (value_dices_numbers.four == 5) {
		/* Five Fours */
		result_number = 1.3;
	} else if ((value_dices_numbers.one == 5) || (value_dices_numbers.two == 5) || (value_dices_numbers.three == 5) || (value_dices_numbers.five == 5) || (value_dices_numbers.six == 5)) {
		/* Five of a Kind */
		result_number = 1.4;
	} else if (value_dices_numbers.four == 4) {
		/* Four Fours */
		result_number = 1.5;
	} else if ((value_dices_numbers.one == value_dices_numbers.two) && (value_dices_numbers.two == value_dices_numbers.three) && (value_dices_numbers.four == value_dices_numbers.three) && (value_dices_numbers.four == value_dices_numbers.five) && (value_dices_numbers.five == value_dices_numbers.six)) {
		/* Straight */
		result_number = 2;
	} else if ((value_dices_numbers.one == 4) || (value_dices_numbers.two == 4) || (value_dices_numbers.three == 4) || (value_dices_numbers.five == 4) || (value_dices_numbers.six == 4)) {
		/* Four of a Kind */
		result_number = 3;
	} else if ((value_dices_numbers.four) && (value_dices_numbers.four <= 3)) {
		if (value_dices_numbers.four == 3) {
			/* Three Fours */
			result_number = 4;
		} else if (value_dices_numbers.four == 2) {
			/* Two Fours */
			result_number = 5;
		} else {
			/* One Four */
			result_number = 6;
		}
	} else {
		let count_threes = 0;
		
		/* Three of a Kind */
		if (value_dices_numbers.one == 3) {count_threes++};
		if (value_dices_numbers.two == 3) {count_threes++};
		if (value_dices_numbers.three == 3) {count_threes++};
		if (value_dices_numbers.five == 3) {count_threes++};
		if (value_dices_numbers.six == 3) {count_threes++};
		
		if (count_threes == 2) {
			result_number = 2.1;
		};
	};
	
	let results = {'dice': value_dices,
		'result': result_number};
	return results;
}
