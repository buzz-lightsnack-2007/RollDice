/* interface.js
 script for the interface */

let soundEffects = {
	'ding': new Audio('media/ding.mp4'),
	'hit': new Audio('media/Shells_falls-Marcel-829263474.mp3'),
	'tada': new Audio('media/tada.mp3')
}

function app_dice_appearanceToggle(setting = true) {
	if (setting) {
		$('.dice').removeClass("scale-out");
	} else {
		$('.dice').addClass("scale-out");
	}
};

function app_about_appearanceToggle() {
	let aboutMessage = (app.name.toUpperCase() + ': ' + app.description + '\n\n' + 'Maintained by ' + app.authors + '.\n' + 'Source code available at: ' + app.sourcecode);
	
	console.log(aboutMessage);
	alert(aboutMessage);
}
